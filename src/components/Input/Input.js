import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changeInput, fetchTodo, getTodo} from "../../store/actions";

const Input = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.input);



    const change = (e) => dispatch(changeInput(e.target.value));
    const postRequest = (e) => {
        e.preventDefault();
        dispatch(fetchTodo(posts));
        dispatch(getTodo());
    } ;

    return (
        <>
        <form onSubmit={(e) => postRequest(e)}>
        <div className="input-group mb-3 mt-5">
                    <input type="text" className="form-control" onChange={change} placeholder="Recipient's username"
                           aria-label="Recipient's username" aria-describedby="button-addon2"/>
                    <button  className="btn btn-outline-secondary" type="submit" id="button-addon2">Button</button>
            </div>
        </form>
        </>
    );
};

export default Input;
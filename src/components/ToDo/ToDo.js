import React from 'react';

const ToDo = ({text, btn}) => {
    return (
        <li className='list-group-item d-flex justify-content-between'>
            {text}
            <button className='btn btn-primary'>{btn}</button>
        </li>
    );
};

export default ToDo;
import React, {useEffect} from 'react';
import './Counter.css';
import {useDispatch, useSelector} from "react-redux";
import {add, fetchCounter, decrease, increase, putCounter, subtract} from "../../store/actions";

const Counter = () => {
    const dispatch = useDispatch();
    const counter = useSelector(state => state.counter);

    useEffect(() => {
        dispatch(fetchCounter());
    }, [dispatch]);

    const increaseCounter = () => {
        dispatch(increase());
        dispatch(putCounter(counter + 1))
    };
    const AddCounter = () => {
        dispatch(add(5));
        dispatch(putCounter(counter + 5))
    };
    const del = () => {
        dispatch(decrease());
        dispatch(putCounter(counter - 1))
    };

    const minusCounter = () => {
        dispatch(subtract(5));
        dispatch(putCounter(counter - 5))
    }


    return (
        <div className='Counter'>
            <h1>{counter}</h1>
            <button onClick={increaseCounter}>Increase</button>
            <button onClick={del}>Decrease</button>
            <button onClick={AddCounter}>Increase by 5</button>
            <button onClick={minusCounter}>Decrease by 5</button>
        </div>
    );
};

export default Counter;
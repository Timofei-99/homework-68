import React from 'react';
import  './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className='head'>
            <ul className='items'>
                <li>
                    <NavLink exact to={'/'}>Task 1</NavLink>
                </li>
                <li>
                    <NavLink to={'/task2'}>Task 2</NavLink>
                </li>
            </ul>
        </header>
    );
};

export default Header;
import axios from "axios";

const axiosAPI = axios.create({
    baseURL: 'https://pages-5e5ed-default-rtdb.firebaseio.com/',
});

export default axiosAPI;
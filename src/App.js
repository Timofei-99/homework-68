import MainPage from "./containers/MainPage/MainPage";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Header from "./components/UI/Header/Header";
import Counter from "./components/Counter/Counter";

function App() {
    return (
        <BrowserRouter>
            <Header/>
            <div className='container'>
                <Switch>
                    <Route exact path={'/'} component={MainPage}/>
                    <Route path={'/task2'} component={Counter}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;

import React, {useEffect} from 'react';
import Input from "../../components/Input/Input";
import {useDispatch, useSelector} from "react-redux";
import {getTodo} from "../../store/actions";
import ToDo from "../../components/ToDo/ToDo";

const MainPage = () => {
    const posts = useSelector(state => state.toDo);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getTodo())
    }, [dispatch]);


    return (
        <>
            <Input/>
            <ul>
                {posts.map((p) => {
                    return (
                        <ToDo
                            key={Object.keys(p)[0]}
                            text={p[Object.keys(p)[0]].text}
                            btn="Delete"
                        />
                    )
                })}
            </ul>
        </>
    );
};

export default MainPage;
import {
    FETCH_TODO_FAILURE,
    FETCH_TODO_REQUEST,
    FETCH_TODO_SUCCESS,
    GET_TODO_FAILURE,
    GET_TODO_REQUEST,
    GET_TODO_SUCCESS,
    INPUT,
    INCREASE,
    ADD,
    DECREASE,
    SUBTRACT, FETCH_COUNTER_REQUEST, FETCH_COUNTER_SUCCESS, FETCH_COUNTER_FAILURE
} from "./actions";

const initialState = {
    toDo: [],
    input: '',
    counter: 0,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT:
            return {...state, input: action.payload};
        case FETCH_TODO_REQUEST:
            return {...state};
        case FETCH_TODO_SUCCESS:
            return {...state, toDo: [...state.toDo, action.payload]};
        case FETCH_TODO_FAILURE:
            return {...state}
        case GET_TODO_REQUEST:
            return {...state}
        case GET_TODO_SUCCESS:
            return {
                ...state, toDo: Object.keys(action.payload).map(key => {
                    return {[key]: action.payload[key]}
                })
            }
        case GET_TODO_FAILURE:
            return {...state}
        case INCREASE:
            return {...state, counter: state.counter + 1};
        case ADD:
            return {...state, counter: state.counter + action.payload}
        case DECREASE:
            return {...state, counter: state.counter - 1}
        case SUBTRACT:
            return {...state, counter: state.counter - action.payload}
        case FETCH_COUNTER_REQUEST:
            return {...state}
        case FETCH_COUNTER_SUCCESS:
            return {...state, counter: action.payload}
        case FETCH_COUNTER_FAILURE:
            return {...state}
        default:
            return state
    }
};

export default reducer;
import axiosAPI from "../axiosAPI";

export const INPUT = 'INPUT';
export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_FAILURE = 'FETCH_TODO_FAILURE';

export const GET_TODO_REQUEST = 'GET_TODO_REQUEST';
export const GET_TODO_SUCCESS = 'GET_TODO_SUCCESS';
export const GET_TODO_FAILURE = 'GET_TODO_FAILURE';


export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const PUT_COUNTER_REQUEST = 'PUT_COUNTER_REQUEST';
export const PUT_COUNTER_SUCCESS = 'PUT_COUNTER_SUCCESS';
export const PUT_COUNTER_FAILURE = 'PUT_COUNTER_FAILURE';


export const changeInput = e => ({type: INPUT, payload: e});

export const fetchToDoRequest = () => ({type: FETCH_TODO_REQUEST})
export const fetchToDoSuccess = (todo) => ({type: FETCH_TODO_SUCCESS, payload: todo});
export const fetchToDoFailure = () => ({type: FETCH_TODO_FAILURE});


export const increase = () => ({type: INCREASE});
export const decrease = () => ({type: DECREASE});
export const add = value => ({type: ADD, payload: value});
export const subtract = value => ({type: SUBTRACT, payload: value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload: counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});


export const fetchTodo = (arg) => {
    return async (dispatch, getState) => {
        console.log(arg)
        dispatch(fetchToDoRequest());
        const done = {
            text: arg
        };
        try {

            const response = await axiosAPI.post('tasks.json', done);
            dispatch(fetchToDoSuccess({[response.data.name]: done}));
        } catch (e) {
            dispatch(fetchToDoFailure());
        }
    }
};

export const getToDoRequest = () => ({type: GET_TODO_REQUEST});
export const getTodoSuccess = (post) => ({type: GET_TODO_SUCCESS, payload: post});
export const getToDoFailure = () => ({type: GET_TODO_FAILURE});

export const getTodo = () => {
    return async (dispatch, getState) => {
        dispatch(getToDoRequest());

        try {
            const response = await axiosAPI.get('tasks.json');
            if (response.data !== null) {
                dispatch(getTodoSuccess(response.data));
            }
        } catch (e) {
            dispatch(getToDoFailure());
        }
    }
};

export const fetchCounter = () => {
    return async (dispatch, getState) => {
        dispatch(fetchCounterRequest());
        try {
            const response = await axiosAPI.get('count.json');
            dispatch(fetchCounterSuccess(response.data))
        } catch (e) {
            dispatch(fetchCounterFailure())
        }
    }
};

export const putCounterRequest = () => ({type: PUT_COUNTER_REQUEST});
export const putCounterSuccess = count => ({type: PUT_COUNTER_SUCCESS, payload: count});
export const putCounterFailure = () => ({type: PUT_COUNTER_FAILURE});

export const putCounter = (arg) => {
    return async (dispatch, getState) => {
        dispatch(putCounterRequest());
        try {
            const put = await axiosAPI.put('count.json', arg);
            dispatch(putCounterSuccess(put.data));
        } catch (e) {
            dispatch(putCounterFailure())
        }
    }
}